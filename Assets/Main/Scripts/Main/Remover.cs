﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Remover : MonoBehaviour
{
    #region Private Variable
    private const int PLAYER_LAYER = 9;
    #endregion

    #region Event Functions
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer != PLAYER_LAYER)
        collision.gameObject.SetActive(false);
    }
    #endregion
}
