﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBehaviour : MonoBehaviour
{
    #region Public Variable
    public float m_speed;
    #endregion

    #region Private Variable
    private Rigidbody2D m_rigidbody;
    #endregion

    #region Unity Callbacks
    // Start is called before the first frame update
    void Start()
    {
        Prepare();
    }

    // Update is called once per frame
    void Update()
    {
        var currentPosition = transform.position;

        transform.position = new Vector2(currentPosition.x - Speed(), currentPosition.y);
    }
    #endregion

    #region Helping Functions
    private void Prepare()
    {
        m_rigidbody = transform.GetComponent<Rigidbody2D>() == null
            ? this.gameObject.AddComponent<Rigidbody2D>()
            : transform.GetComponent<Rigidbody2D>();

        m_rigidbody.gravityScale = 0;

        if (transform.GetComponent<BoxCollider2D>() == null)
        {
            this.gameObject.AddComponent<BoxCollider2D>();
        }
    }

    private float Speed()
    {
        return m_speed * Time.deltaTime;
    }
    #endregion
}
