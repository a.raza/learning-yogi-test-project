﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : Singleton<BackgroundManager>
{
    #region Public Variable
    public float m_speed;
    #endregion

    #region Private Variable
    private Renderer m_renderer;
    #endregion

    #region Unity Callbacks
    private void Awake()
    {
        Prepare();
    }

    // Update is called once per frame
    void Update()
    {
        m_renderer.material.mainTextureOffset += new Vector2(Speed(), 0f);
    }
    #endregion

    #region Helping Functions
    private void Prepare()
    {
        if (transform.GetComponent<MeshRenderer>() == null)
        {
            Debug.LogError("No MeshRenderer Attached");
            Stop();
            return;
        }
        else
        {
            m_renderer = transform.GetComponent<MeshRenderer>();
        }
    }

    private float Speed()
    {
        return m_speed * Time.deltaTime;
    }

    public void Stop()
    {
        this.enabled = false;
    }
    #endregion
}
