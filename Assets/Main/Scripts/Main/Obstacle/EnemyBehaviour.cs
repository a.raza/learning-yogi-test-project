﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBehaviour : MonoBehaviour
{
    #region Private Variable
    private const string ANIMATION_KEY = "IsAlive";
    private const int PLAYER_LAYER = 9;
    private Animator m_anim;
    private PlayerBehaviour m_player;
    private GameManager m_gameManager;
    #endregion

    #region Public Variable
    public AudioSource[] m_audioSources;
    #endregion

    #region Unity Callbacks

    private void OnEnable()
    {
        Prepare();
        AnimationState(true);
    }
    // Start is called before the first frame update
    protected virtual void Start()
    {
        //Prepare();
    }
    #endregion

    #region Event Functions
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == PLAYER_LAYER)
        {
            if (m_player == null)
                m_player = FindObjectOfType<PlayerBehaviour>();

            m_player.SetAnimation(State.HURT);

            if (m_gameManager == null)
                m_gameManager = FindObjectOfType<GameManager>();

            StartCoroutine(m_gameManager.EnableGameOver());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == PLAYER_LAYER)
        {
            AnimationState(false);
            if (m_player == null)
                m_player = FindObjectOfType<PlayerBehaviour>();

            m_player.Jump();
            if(!m_player.IsActing(State.HURT))
            {
                if (m_gameManager == null)
                    m_gameManager = FindObjectOfType<GameManager>();

                m_gameManager.AddScore(PointsValue.ENEMY);
            }
        }
    }

    public void Die()
    {
        this.gameObject.SetActive(false);
    }

    public void EnemyAudio(int index)
    {
        if (!m_audioSources[index].isPlaying)
        {
            m_audioSources[index].Play();
        }
    }
    #endregion

    #region Helping Functions
    private void Prepare()
    {
        if (transform.GetComponent<Rigidbody>() == null)
        {
            this.gameObject.AddComponent<Rigidbody2D>();
        }

        if (transform.GetComponent<Animator>() == null)
        {
            Debug.LogError("No Animator Attached");
            Stop();
            return;
        }
        else
        {
            m_anim = transform.GetComponent<Animator>();
        }

        if (transform.GetComponent<BoxCollider2D>() == null)
        {
            this.gameObject.AddComponent<BoxCollider2D>();
        }

        m_gameManager = GameManager.Instance;
        m_player = PlayerBehaviour.Instance;
    }

    public void Stop()
    {
        this.enabled = false;
    }

    public void AnimationState(bool isAlive)
    {
        m_anim.SetBool(ANIMATION_KEY, isAlive);
    }
    #endregion

}
