﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    #region Private Variable
    private const string
        SCORE_KEY = "SUNNY_SCORE",
        MENU = "[MenuHolder]",
        PAUSE_MENU = "[PauseMenu]",
        PAUSE_BUTTON = "[PauseButton]",
        TITLE = "[Holder]/[Title]",
        RESUME = "[Holder]/[Resume]",
        RETRY = "[Holder]/[Retry]",
        EXIT = "[Holder]/[Exit]",
        HEADER = "[Header]",
        SCORE = "[Score]",
        BEST_SCORE = "[Holder]/[BestScore]",
        MY_SCORE = "[Holder]/[MyScore]";

    private Transform
        m_pauseMenu,
        m_header;

    private Button
        m_pause,
        m_resume,
        m_retry,
        m_exit;

    private TMPro.TextMeshProUGUI
        m_title,
        m_score,
        m_highScore,
        m_myScore;

    private int m_bestScore = 0;
    private int m_currentScore = 0;
    #endregion

    #region Unity Callbacks
    private void Awake()
    {
        Prepare();
        LoadScore();
    }
    #endregion

    #region Helping Functions
    private void Prepare()
    {
        var menu = GameObject.Find(MENU).transform;

        if (menu == null)
        {
            Debug.LogError("MENU HOLDER NOT FOUND");
            this.enabled = false;
            return;
        }

        m_pauseMenu = menu.Find(PAUSE_MENU);

        m_pause = menu.Find(PAUSE_BUTTON).GetComponent<Button>();
        m_pause.onClick.AddListener(() => Pause());

        m_resume = m_pauseMenu.Find(RESUME).GetComponent<Button>();
        m_resume.onClick.AddListener(() => Resume());

        m_retry = m_pauseMenu.Find(RETRY).GetComponent<Button>();
        m_retry.onClick.AddListener(() => Retry());

        m_exit = m_pauseMenu.Find(EXIT).GetComponent<Button>();
        m_exit.onClick.AddListener(() => Exit());

        m_title = m_pauseMenu.Find(TITLE).GetComponent<TMPro.TextMeshProUGUI>();

        m_highScore = m_pauseMenu.Find(BEST_SCORE).GetComponent<TMPro.TextMeshProUGUI>();

        m_myScore = m_pauseMenu.Find(MY_SCORE).GetComponent<TMPro.TextMeshProUGUI>();

        m_header = menu.Find(HEADER);

        m_score = m_header.Find(SCORE).GetComponent<TMPro.TextMeshProUGUI>();
    }


    public void ManageScore()
    {
        if (m_score != null)
            m_score.text = m_currentScore.ToString();
    }

    public void SaveScore()
    {
        if (m_currentScore > m_bestScore)
            m_bestScore = m_currentScore;

        PlayerPrefs.SetInt(SCORE_KEY, m_bestScore);
    }

    private void LoadScore()
    {
        if (PlayerPrefs.HasKey(SCORE_KEY))
        {
            m_bestScore = PlayerPrefs.GetInt(SCORE_KEY);
        }
    }

    public void AddScore(PointsValue value)
    {
        m_currentScore += (int)value;

        ManageScore();
    }

    public IEnumerator EnableGameOver()
    {
        yield return new WaitForSeconds(1.5f);

        m_title.text = "GAME OVER";

        m_resume.gameObject.SetActive(false);

        SaveScore();

        Pause();
    }
    #endregion

    #region Event Functions
    private void Pause()
    {
        m_myScore.text = "My Score: " + m_currentScore;
        m_highScore.text = "Best Score: " + m_bestScore;

        if (m_pauseMenu != null)
            m_pauseMenu.gameObject.SetActive(true);

        Time.timeScale = 0;
    }

    private void Resume()
    {
        if (m_pauseMenu != null)
            m_pauseMenu.gameObject.SetActive(false);

        Time.timeScale = 1f;
    }

    private void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

    private void Exit()
    {
        SaveScore();
        SceneManager.LoadScene(0);
    }
    #endregion
}

public enum PointsValue
{
    ENEMY = 10,
    GEM = 2,
};
