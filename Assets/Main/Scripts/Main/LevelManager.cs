﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    #region Private Variable
    private const float Y_MAX = 1.20f, Y_MIN = -1.75f;
    private ObjectPooling m_pool;
    #endregion

    #region Public Variable
    public GameObject m_platformPrefab;
    public ushort m_platformAmounts;
    public float m_delayForPlatforms;
    public Transform m_startingArea;
    #endregion

    #region Unity Callbacks
    private void Start()
    {
        m_pool = ObjectPooling.Instance;

        if(m_pool == null)
        {
            m_pool = FindObjectOfType<ObjectPooling>();
        }

        m_pool.CreatePool(m_platformPrefab, m_platformAmounts);

        StartCoroutine(ManageLevel());
    }
    #endregion

    #region Helping Functions
    private IEnumerator ManageLevel()
    {
        yield return new WaitForSeconds(m_delayForPlatforms);

        var platform = m_pool.GetReusableObject(m_platformPrefab.name);

        if (platform != null)
        {
            var startingPosition = new Vector3(m_startingArea.position.x, Random.Range(Y_MIN, Y_MAX));

            var rand = Random.Range(0, 5);

            if(rand < 2)
            {
                platform.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                platform.transform.GetChild(1).gameObject.SetActive(true);
            }

            if(rand < 2)
                foreach (Transform child in platform.transform.GetChild(0))
                {
                    child.gameObject.SetActive(true);
                }

            platform.transform.position = startingPosition;

            platform.SetActive(true);
        }
        else
        {
            Debug.Log("platform Not Available");
        }

        StartCoroutine(ManageLevel());
    }
    #endregion
}
