﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : Singleton<PlayerBehaviour>
{
    #region Private Variable
    private const string ANIMATION_KEY = "Sunny";
    private const string JUMP_KEY = "IsJumping";
    private const float OFFSET_X = 1.5f, OFFSET_Y = 1.5f;
    private const int DIAMOND_LAYER = 10 , REMOVER = 11;

    private Rigidbody2D m_rigidbody;
    private GameManager m_gameManager;

    private Animator m_anim;
    #endregion

    #region Public Variable
    public AudioSource[] audioSources;
    public LayerMask m_platformLayerMask;

    public float m_jumpVelocity;
    public bool isGrounded;
    #endregion

    #region Unity Callbacks
    private void OnEnable()
    {
        Prepare();
    }

    // Start is called before the first frame update
    void Start()
    {
        Prepare();
    }

    private void Update()
    {
        IsGrounded();

        // JUMP
        if (!IsActing(State.HURT) && Input.touchCount > 0 /*Input.GetKeyDown(KeyCode.Space)*/ && isGrounded)
        {
            SetAnimation(State.JUMP);

            Jump();
        }
    }
    #endregion

    #region Helping Functions
    public void Jump()
    {
        m_rigidbody.velocity = Vector2.up * m_jumpVelocity;
    }

    private void Prepare()
    {
        m_rigidbody = transform.GetComponent<Rigidbody2D>() == null
            ? this.gameObject.AddComponent<Rigidbody2D>()
            : transform.GetComponent<Rigidbody2D>();

        if (transform.GetComponent<Animator>() == null)
        {
            Debug.LogError("No Animator Attached");
            Stop();
            return;
        }
        else
        {
            m_anim = transform.GetComponent<Animator>();
        }

        if(transform.GetComponent<BoxCollider2D>() == null)
        {
            this.gameObject.AddComponent<BoxCollider2D>();
        }

        m_gameManager = GameManager.Instance;
    }

    private void IsGrounded()
    {
        isGrounded = Physics2D.OverlapArea(new Vector2(transform.position.x - OFFSET_X, transform.position.y - OFFSET_Y),
            new Vector2(transform.position.x + OFFSET_X, transform.position.y - (OFFSET_Y + 0.01f)), m_platformLayerMask);

        if (isGrounded && IsActing(State.JUMP))
        {
            SetAnimation(State.RUN);
        }
    }

    public bool IsActing(State state)
    {
       return m_anim.GetCurrentAnimatorStateInfo(0).IsTag(state.ToString()).Equals(true);
    }

    public void SetAnimation(State state)
    {
        if (!IsActing(state))
        {
            if(state == State.JUMP)
            {
                m_anim.SetInteger(ANIMATION_KEY, 1);
                m_anim.SetBool(JUMP_KEY, true);
            }
            else
            {
                m_anim.SetInteger(ANIMATION_KEY, (int)state);
                m_anim.SetBool(JUMP_KEY, false);
            }
        }
    }

    public void Stop()
    {
        this.enabled = false;
    }
    #endregion

    #region Event Functions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == DIAMOND_LAYER)
        {
            collision.gameObject.SetActive(false);
            if(!IsActing(State.HURT))
            {
                if (m_gameManager == null)
                    m_gameManager = FindObjectOfType<GameManager>();

                m_gameManager.AddScore(PointsValue.GEM);
                SunnyAudio(2);
            }
        }

        if(collision.gameObject.layer == REMOVER)
        {
            if (m_gameManager == null)
                m_gameManager = FindObjectOfType<GameManager>();

            SunnyAudio(0);
            StartCoroutine(m_gameManager.EnableGameOver());
        }
    }

    public void SunnyAudio(int index)
    {
        if (!audioSources[index].isPlaying)
        {
            audioSources[index].Play();
        }
    }
    #endregion
}
public enum State
{
    IDLE = 0,
    RUN = 1,
    JUMP = 2,
    HURT = 4,
};
