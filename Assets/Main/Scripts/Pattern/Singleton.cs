﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T m_instance;

    private static object m_lock = new object();

    public static T Instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("  Instance '" + typeof(T));
                return null;
            }

            lock (m_lock)
            {
                if (m_instance == null)
                {
                    m_instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogError("  Should Only Be 1 singleton!");
                        return m_instance;
                    }

                    if (m_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        m_instance = singleton.AddComponent<T>();
                        singleton.name = "(singleton) " + typeof(T).ToString();
                    }
                    else
                    {
                        Debug.Log("  Using instance already created: " +
                            m_instance.gameObject.name);
                    }
                }

                return m_instance;
            }
        }
    }

    public static void ForceInstance(T instance)
    {
        m_instance = instance;
    }

    private static bool applicationIsQuitting = false;

    public void OnDestroy()
    {
        applicationIsQuitting = true;
    }
}