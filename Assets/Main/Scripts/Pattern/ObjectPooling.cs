﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : Singleton<ObjectPooling>
{
    #region Private Variable
    private List<ReusableObject[]> m_pooledObjectList = new List<ReusableObject[]>();
    #endregion

    #region Helping Functions
    public void CreatePool(GameObject prefab, ushort amount)
    {
        ReusableObject[] reusableObjects = new ReusableObject[amount];

        for(int i = 0; i < amount; i++)
        {
            var reusableObject = Instantiate(prefab);

            reusableObjects[i] = new ReusableObject
            {
                m_name = prefab.name,
                m_reusableObject = reusableObject
            };

            reusableObject.SetActive(false);
        }

        m_pooledObjectList.Add(reusableObjects);
    }

    public GameObject GetReusableObject(string name)
    {
        foreach(var arrayOfReuseables in m_pooledObjectList)
        {
            if (arrayOfReuseables[0].m_name == name)
            {
                return FindObjectToReuse(arrayOfReuseables);
            }
        }

        return null;
    }

    private GameObject FindObjectToReuse(ReusableObject[] reusableObjects)
    {
        foreach(var reuseable in reusableObjects)
        {
            if (!reuseable.m_reusableObject.activeInHierarchy)
            {
                reuseable.m_reusableObject.SetActive(true);
                return reuseable.m_reusableObject;
            }
        }

        return null;
    }
    #endregion
}

[System.Serializable]
public class ReusableObject
{
    public string m_name;
    public GameObject m_reusableObject;
}
